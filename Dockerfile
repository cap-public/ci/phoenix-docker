FROM hexpm/elixir:1.11.1-erlang-23.1.1-ubuntu-focal-20200703

RUN apt-get update \
    && DEBIAN_FRONTEND="noninteractive" TZ="Etc/UTC" apt-get -y install tzdata \
    && apt-get -qq install -y git build-essential libssl-dev curl wget software-properties-common nodejs npm

# Cypress dependencies
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb

RUN mix local.hex --force \
    && mix local.rebar --force
